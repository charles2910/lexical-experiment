# Mental Lexicon Experiment

This project aims at providing the Webpage for the mental lexicon experiment
conducted in the research "Spelling–sound knowledge in the context of
multilingualism: is lexical access selective or nonselective?"
