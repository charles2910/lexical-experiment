FROM debian:stable
RUN apt update &&  echo exit 0 > /usr/sbin/policy-rc.d && apt install -y nginx-light
WORKDIR /var/www/html
RUN rm /var/www/html/index.nginx-debian.html
RUN service nginx restart
